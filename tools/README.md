# Utility Scripts

## Catppuccin

- [`catppuccin-template-gen.py`](https://gitlab.com/blackcat-linux/blackcat-repo/-/blob/main/scripts/catppuccin-template-gen.py): Used to batch generate templates for each flavor of Catppuccin and a meta packages for all Catppuccin themes.
- [`generate-hash.sh`](https://gitlab.com/blackcat-linux/blackcat-repo/-/blob/main/scripts/generate-hash.sh): Used to generate and update the checksums of the given package.


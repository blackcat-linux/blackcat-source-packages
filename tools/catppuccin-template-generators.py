#!/usr/bin/env python3

import os
from pathlib import Path

TEMPLATES = str(Path.cwd().parent.absolute().joinpath("srcpkgs"))
print(f"{TEMPLATES=}")

types = [
    "gtk",
    "cursors",
]
flavors = [
    "frappe",
    "latte",
    "macchiato",
    "mocha",
]
colors = [
    "blue",
    "dark",
    "flamingo",
    "green",
    "lavender",
    "light",
    "maroon",
    "mauve",
    "peach",
    "pink",
    "red",
    "rosewater",
    "sapphire",
    "sky",
    "teal",
    "yellow",
]

MAINTAINER = "TheWalkingForest <4783673-TheWalkingForest@users.noreply.gitlab.com>"

META_NAME_CURSOR = "catppuccin-cursor-themes"
HOME_PAGE_CURSOR = "https://github.com/catppuccin/cursor"
DESC_CURSOR = "Soothing pastel cursors GTK/Plasma/Hyprland"
VERSION_CURSOR = "1.0.0"
LICENSE_CURSOR = "GPL-2.0-only"
CURSORS = "cursors"


HOME_PAGE_GTK = "https://github.com/catppuccin/gtk"
META_NAME_GTK = "catppuccin-gtk-themes"
DESC_GTK = "Soothing pastel theme for the high-spririted"
VERSION_GTK = "1.0.3"
LICENSE_GTK = "GPL-3.0-or-later"
GTK = "gtk"


def flavor_name(flavor: str, kind: str) -> str:
    return f"catppuccin-{flavor}-{kind}"


def generate_meta_template(
    meta_name: str, homepage: str, kind: str, description: str, version: str
):
    outdir = os.path.join(TEMPLATES, meta_name)
    print(f"{outdir=}")
    if not os.path.exists(outdir):
        os.mkdir(outdir)
    template = os.path.join(outdir, "template")
    print(f"{template=}")
    with open(template, "w") as outfile:
        outfile.write(
            f"# Template file for '{meta_name}'\n"
            f"pkgname={meta_name}\n"
            f"version={version}\n"
            "revision=1\n"
            "build_style=meta\n"
            'depends="'
        )
        depends = []
        for f in flavors:
            depends.append(flavor_name(f, kind) + ">=${version}")
        outfile.write("\n\t".join(depends))
        outfile.write('"')

        outfile.write(
            f'short_desc="{description} - Meta package"\n'
            f'maintainer="{MAINTAINER}"\n'
            'license="Public Domain"\n'
            f'homepage="{homepage}"\n'
        )


def generate_flavor_templates(
    kind: str, version: str, osi_license: str, home_page: str
):
    for flavor in flavors:
        outdir = os.path.join(TEMPLATES, flavor_name(flavor, kind))
        print(f"{outdir=}")
        if not os.path.exists(outdir):
            os.mkdir(outdir)

        template = os.path.join(outdir, "template")
        print(f"{template=}")

        with open(template, "w") as outfile:
            outfile.write(
                f"# Template file for '{flavor_name(flavor, kind)}'\n"
                f"pkgname={flavor_name(flavor, kind)}\n"
                f"version={version}\n"
                "revision=1\n"
                f'short_desc="Catppuccin {kind} theme - {flavor}"\n'
                'maintainer="TheWalkingForest <4783673-TheWalkingForest@users.noreply.gitlab.com>"\n'
                f'license="{osi_license}"\n'
                f'homepage="{home_page}"\n'
                'distfiles="'
            )

            distfiles = []
            for color in colors:
                if kind == CURSORS:
                    distfiles.append(
                        f"https://github.com/catppuccin/{kind}/releases/download/v${{version}}/catppuccin-{flavor}-{color}-{kind}.zip"
                    )
                if kind == GTK and (color != "light" and color != "dark"):
                    distfiles.append(
                        f"https://github.com/catppuccin/{kind}/releases/download/v${{version}}/catppuccin-{flavor}-{color}-standard+default.zip"
                    )
            outfile.write("\n\t".join(distfiles))
            outfile.write('"')
            outfile.write(
                "\nchecksum=badbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadbadb\n\n"
            )

            if kind == CURSORS:
                outfile.write(
                    "do_install() {\n"
                    "\tvmkdir usr/share/icons\n"
                    "\tvcopy . usr/share/icons\n"
                    "}\n"
                )
            if kind == GTK:
                outfile.write(
                    "do_install() {\n"
                    "\tvmkdir usr/share/themes\n"
                    "\tvcopy . usr/share/themes\n"
                    "}\n"
                )


def main():
    generate_meta_template(
        META_NAME_CURSOR, HOME_PAGE_CURSOR, CURSORS, DESC_CURSOR, VERSION_CURSOR
    )
    generate_meta_template(META_NAME_GTK, HOME_PAGE_GTK, GTK, DESC_GTK, VERSION_GTK)
    generate_flavor_templates(CURSORS, VERSION_CURSOR, LICENSE_CURSOR, HOME_PAGE_CURSOR)
    generate_flavor_templates(GTK, VERSION_GTK, LICENSE_GTK, HOME_PAGE_GTK)
    return


if __name__ == "__main__":
    main()

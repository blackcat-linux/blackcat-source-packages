#!/usr/bin/env sh

readonly VOID_PACKAGES="https://github.com/void-linux/void-packages"
readonly VOID_PACKAGES_DIR="https://github.com/void-linux/void-packages"
readonly MINI_BUILDER="https://raw.githubusercontent.com/the-maldridge/xbps-mini-builder/master/xbps-mini-builder"

set -

cd .. || exit 1

mkdir builddir

cd builddir || exit 1

wget "$MINI_BUILDER"
git clone --depth=1 "$VOID_PACKAGES" "$VOID_PACKAGES_DIR"

cp -r ../srcpkgs/* "$VOID_PACKAGES_DIR"/srcpkgs

ls ../srcpkgs > packages.list

if [ ! -f "../xbps-src.conf" ]; then
    cp "void-packages/etc/defaults.conf" "xbps-src.conf"
fi

chmod +x xbps-mini-builder

./xbps-mini-builder

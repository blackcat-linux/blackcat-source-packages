readonly BLACKCAT_REPO=${BLACKCAT_REPO:-$(readlink --canonicalize "${0%/*}"/..)}
readonly VOID_PKGS=${VOID_PKGS:-"$HOME/void-packages"}

if ! test -d "$VOID_PKGS"; then
    echo "ERROR: VOID_PKGS dir '$VOID_PKGS' does not exist"
    exit 1
fi
if ! test -d "$BLACKCAT_REPO"; then
    echo "ERROR: BLACKCAT_REPO dir '$BLACKCAT_REPO' does not exist"
    exit 1
fi

echo "BLACKCAT_REPO=$BLACKCAT_REPO"
echo "VOID_PKGS=$VOID_PKGS"

for i in "$BLACKCAT_REPO/srcpkgs"/*; do
    echo "$i" "$VOID_PKGS/srcpkgs/$(basename "$i")"
    cp -r "$i" "$VOID_PKGS/srcpkgs"
done

#!/bin/bash

readonly BLACKCAT_REPO=${BLACKCAT_REPO:-$(readlink --canonicalize "${0%/*}"/..)}

usage() {
    printf "USAGE: %s <PKG>\n" "${0}"
}

main() {
    TMP=${TMP:-"$(mktemp -d -p /tmp "bcl-pkg-hasher-$(date +"%s").XXXXXX")"}
    for p in "$@"; do
        PKG="$p"
        echo "=> ${PKG}"
        FILE="${BLACKCAT_REPO}/srcpkgs/${PKG}/template"
        if [ ! -f "${FILE}" ]; then
            echo "ERROR: ${PKG} doesn't exist"
            usage
            exit 1
        fi

        VERSION="$(grep ^version "${FILE}" | cut -d'=' -f2)"

        DIST_FILES=$(grep ^distfiles "${FILE}" | cut -d"=" -f2 | sed s/\"//g)

        FULL_URIS=()
        read -ra FULL_URIS <<< "${DIST_FILES//\$\{version\}/"$VERSION"}"
        parallel -j4 -0 --bar "wget -P ${TMP}/${PKG} {}" ::: "${FULL_URIS[@]}"

        CHECKSUMS="$(sha256sum "${TMP}/${PKG}"/* | cut -f1 -d ' ' | xargs)"

        cs=$(grep ^checksum "${FILE}" | cut -d"=" -f2 | xargs)
        echo "${cs}"
        sed -i "s|${cs}|${CHECKSUMS}|" "$FILE"

        echo "template file: ${FILE}"
        echo "tmpfile:      ${TMP}"
        echo "version:      ${VERSION}"
        echo "dist files:   ${DIST_FILES}"
        echo "full uri:     ${FULL_URIS[*]}"
        echo "old checksum: ${cs}"
        echo "new checksum: ${CHECKSUMS}"
    done
}

main "$@"
